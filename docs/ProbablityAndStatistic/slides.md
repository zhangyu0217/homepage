---
title : slides
---


[Chapter1](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ProbablityAndStatistic/chap1.pptx?inline=false)


[Chapter2](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ProbablityAndStatistic/chap2.pptx?inline=false)

[Chapter3](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ProbablityAndStatistic/chap3.pptx?inline=false)

[Book : CHEN Xiru](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ProbablityAndStatistic/book_ChenXiru.pdf?inline=false)
