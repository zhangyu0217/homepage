---
title : Slides
---


[Chap0](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/chap0.pdf?inline=false)

[Chap1](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/chap1.pdf?inline=false)

[Chap2](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/chap2.pdf?inline=false)

[Chap3](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/chap3.pdf?inline=false)

[Chap4](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/chap4.pdf?inline=false)
[Homework](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/homework.pdf?inline=false)

[Textbook](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/textbook.pdf?inline=false)

[Xshell Installer](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ComputationalPhysics/xshell7.0.zip)
