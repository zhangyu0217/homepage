<center><font size=10>Yu Zhang</font></center><!--![photon](image.jpg)-->
<center>tel: +86 187 1010 7662 | mail: yu.zhang@cern.ch | skype: yu.zhang0217 | birth: 17 Feb 1991 | gender: male</center>
# **Education**
 * **University of Science and Technology of China** <p align="right">Aug 2010 - Jun 2014</p>
     -  Bachelor in Nuclear and Particle Physics, School of Physics <p align="right">Hefei</p>
 * **Institute of High Energy Physics, Chinese Academy of Scieces** <p align="right">Aug 2014 - May 2020</p>
     - PhD in Nuclear and Particle Physics <p align="right">Beijing</p>

# **Employment**

 * Postdoc on CMS experiment in Fudan University China <p align="right">Jul 2020 - now</p>
 * Fellowship of China-Germany Postdoc Exchange Program <p align="right">Nov 2021 - Nov 2023</p>

# **Rearch Experience**
## ATLAS Experiment
 * **The first observation of VBF Higgs via diphoton final state on ATLAS detector** <p align="right">Jul 2015 - Apr 2019</p>
     - Play a leading role in Higgs to $\gamma\gamma$ analysis : optimize the selection of VBF category with BDT method, edit the internal note and maintain the DAOD production
     - The measured significance of VBF production mode in diphoton final state is 4.9$\sigma$.
     - Develope new technique(quark/gluon jet tagging) to supress gluon-gluon fusion background.
 * Search for Higgs pair production in WW$\gamma\gamma$ final state on ATLAS detector <p align="right">Oct 2016 - Mar 2018</p>
     - Study the background modeling with spurious signal and provide the background modeling uncertainty.
     - Estimate the theoretical uncertainty, including QCD scale, PDF and parton shower uncertainty.
 * Search for Higgs pair production in WWWW final state on ATLAS detector <p algin="right">May 2016 - Oct 2018</p>
     - Combine the three sub-channels (same sign two leptons, three leptons and four leptons), perform the nuisance parameter check (correlation check, pull check and importance check) and set the upper limit. Present Higgs approval talk.
## CMS Experiment
 * Search for four top quarks in $\tau$ final states
 * Study on MET performance
 * Study on HGCal reconstruction algorithm

# **Miscellanous**

# **Conference Talk**

# **Pulication List**

## Journal Paper
 * Measurements of Higgs boson properties in the diphoton decay channel with 36 $fb^{-1}$ of pp collision datat $\sqrt{s}$=13 TeV with the ATLAS detector, Phys.Rev. D98 (2018) 052005 [Link](https://inspirehep.net/literature/1654582)
 * Search for Higgs boson pair production in the $\gamma\gamma$WW* channel using pp collision data recorded a$\sqrt{s}$=13 TeV with the ATLAS detector, Eur.Phys.J. C78 (2018) no.12, 1007 [Link](https://inspirehep.net/literature/1683431)
 * Search for Higgs boson pair production in the WW*WW* decay channel using ATLAS data recorded at $\sqrt{s}$=13 TeV , JHEP [Link](https://inspirehep.net/literature/1705396)
##Conference Note
 * Measurements of Higgs boson properties in the diphoton decay channel using 80 $fb^{-1}$ of pp collision data at $\sqrt{s}$=13 TeV with the ATLAS detector, ICHEP2018 [Link](https://inspirehep.net/literature/1681315)
 * Measurements of Higgs boson properties in the diphoton decay channel with 36.1 $fb^{-1}$ pp collision data at the center-of mass energy of 13 TeV with the ATLAS detector, EPS2017 [Link](https://inspirehep.net/literature/1609522)
 * Measurement of fiducial, differential and production cross sections in the $H\rightarrow\gamma\gamma$ decay channel with 13.3 $fb^{-1}$ of 13 TeV proton-proton collision data with the ATLAS detector , ICHEP2016 [Link](https://inspirehep.net/literature/1480047)
